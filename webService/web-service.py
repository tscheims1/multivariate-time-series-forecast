import datetime
import json
from flask import Flask, request
from ForecastModel import ForecastModel
from PredictionResult import PredictionResult
from TimeSeriesPlot import TimeSeriesPlot

app = Flask(__name__)


@app.route('/forecast', methods=['POST'])
def forecast():
    data = request.get_json()

    for i in range(len(data['InputSequence'])):
        data['InputSequence'][i]['Shipment Date'] = datetime.datetime.strptime(data['InputSequence'][i]['ShipmentDate'],
                                                                               '%Y-%m-%d')
        del data['InputSequence'][i]['ShipmentDate']
    forecast_model = ForecastModel(data['ModelConfig'])
    forecast_data, prediction_sequence = forecast_model.forecast(data['InputSequence'], data['ItemNo'], data['Steps'])

    prediction_result = PredictionResult()
    prediction_result.prediction_sequence = prediction_sequence
    prediction_result.input_sequence = [seq['OrderedQuantity'] for seq in data['InputSequence']]
    if data['TestSequence'] is not None:
        prediction_result.test_sequence = data['TestSequence']
    ts_plot = TimeSeriesPlot()

    return json.dumps({'PredictionSequence': forecast_data, 'Plot': ts_plot.get_base64_plot(prediction_result)})


if __name__ == '__main__':
    app.run(debug=True, host="192.168.178.150")

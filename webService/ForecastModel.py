import pickle

import threading
import pandas as pd
import datetime
import numpy as np
from WindowGenerator import WindowGenerator

lock = threading.Lock()


class ForecastModel:
    instance = None

    def __init__(self, config_name):
        self.config_name = config_name

    def forecast(self, data, item_no, steps):
        df = pd.DataFrame(data)
        forecast_sequence = pd.DataFrame()
        max_date = df['Shipment Date'].max().to_pydatetime()
        for i in range(1, steps + 1):
            max_date + datetime.timedelta(days=i)
            new_df = pd.DataFrame(np.nan, index=range(len(df), len(df) + 1), columns=df.columns)

            new_df['Shipment Date'] = (max_date + datetime.timedelta(days=i))
            new_df = new_df.fillna(0)
            new_df['ItemNo'] = item_no
            forecast_sequence = forecast_sequence.append(new_df, ignore_index=True)
            df = df.append(new_df, ignore_index=True)

        forecast = self._forecast(df, item_no, steps)
        for i in range(steps):
            forecast_sequence.at[i, 'OrderedQuantity'] = forecast[i][0]
        forecast_sequence['ShipmentDate'] = forecast_sequence['Shipment Date'].dt.strftime('%Y-%m-%d')
        forecast_sequence = forecast_sequence.drop(['Shipment Date'], axis=1)

        return forecast_sequence[['ShipmentDate', 'OrderedQuantity']].to_json(orient="records"), [seq[0] for seq in
                                                                                                  forecast]

    def _forecast(self, data, item_no, steps):
        with open("./model-config/" + self.config_name + ".pickle", "rb") as handle:
            model_config = pickle.load(handle)
            with open("./models/" + model_config.model_name + ".pickle", "rb") as h:
                model = pickle.load(h)

                with open('folds1v2_without_weather_with_nans.pickle', "rb") as f:
                    f = pickle.load(f)
                    wg = None
                    for frame in f[0].frames:
                        if frame.item_no == item_no and frame.wg is not None:
                            wg = frame.wg
                            wg.item_no = item_no
                            wg.set_train_data(frame.train_data)
                            wg.set_test_data(data)
                            break
                    if wg is None:
                        wg = WindowGenerator(data, data, item_no=item_no, lookback=model_config.lookback, steps=steps,
                                             use_standard_scaling=True, embedding=model_config.embedding)

                    wg.calc_window()
                    if model_config.is_2_head:
                        t1 = wg.get_feature_tensor_test()
                        t2 = wg.get_feature_tensor2_test()
                        return wg.inverse_transform(model.predict((t1, t2))[0])
                    else:
                        t1 = wg.get_feature_tensor_test()
                        return wg.inverse_transform(model.predict(t1)[0])

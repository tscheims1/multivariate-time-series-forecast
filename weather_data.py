#download weather data of swiss cities from weatherapi.com and store it as json files on the disk

import datetime
import os
import requests

base_dir = "weather"

locations = ["Bern","Basel","Zürich","Lausanne","Lugano","Chur Switzerland","Sion","St. Gallen","Luzern","Delsberg"]
for loc in locations:
    dir = os.path.join(base_dir,loc)
    if not os.path.exists(dir):
        os.mkdir(dir)
        
curr_date = datetime.datetime(2022, 9, 8)
end_date = datetime.datetime(2022, 12, 22)
while curr_date != end_date:
    for loc in locations:
        dir = os.path.join(base_dir, loc)
        file_path = os.path.join(dir, curr_date.strftime("%Y-%m-%d"))+".json"
        r = requests.get('http://api.weatherapi.com/v1/history.json?key=dda8878557574d29869132833220709&q='+loc+'&dt='+curr_date.strftime("%Y-%m-%d"))
        with open(file_path, "w") as text_file:
            text_file.write(r.text)
    print(curr_date,"saved")
    curr_date += datetime.timedelta(days=1)

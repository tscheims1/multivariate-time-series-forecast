# multivariate time series forecast

### Notebooks
- [Dataset analysis](./notebooks/Denner-Dataset-FullNo9-DataSetAnalysis.py.ipynb)

- [Comparsion with naive last, traditional machine learning](./notebooks/Denner-Dataset-FullNo1V3TraditionalVsSrvVSEmbedding.py.ipynb)

- [leak future information in the model (e.g. sales prices, weather forecast information or time dependand information)](./notebooks/Denner-Dataset-FullV5-Use-2-Head-Modelsy.ipynb)

- [Can a overfitted model on the training set detect peaks earlier?](./notebooks/Denner-Dataset-FullV7-show-performance-let-overfit-v2.ipynb)

- [latent space embeddings](./notebooks/Denner-Dataset-FullNo6v3-EmbeddingOfLatentSpace.py.ipynb)

- [different sizes of embedded item number](./notebooks/Denner-Dataset-FullNo10-EmbeddingSize.py.ipynb)

- [curve smoothing as preprocessing step](./notebooks/Denner-Dataset-FullNo3-Curve-SmoothingV3.py.ipynb)

- [how the model performs with less training data](./notebooks/Denner-Dataset-FullNo4-ImpactOfTrainingDataV3.py.ipynb)

- [compare different model architectures](./notebooks/Denner-Dataset-FullNo8-PutEverythingTogether.py.ipynb)

- [impact of using additional weather data](./notebooks/Denner-Dataset-FullV2-WeatherData.py.ipynb)

- [time series clustering](./notebooks/Denner-Dataset-FullNo6v2-TimeSeriesClustering.py.ipynb)

- [zero-shot-learning performance on unseen data (different customer price group with different items)](./notebooks/Denner-Dataset-FullNo8-Generalization.py.ipynb)

### Web Service for integrating in Microsoft Business Central 365
- [web Service source folder](./webService)


### additional source code

- [merging and adding the exported sales shipment lines to a pandas object](pickle-data-full.py)

- [download weather data of swiss cities from weatherapi.com and store it as json files on the disk](weather_data.py)

- [calculate average swiss weather and merge it to existing pandas frames](apply_weather_data_to_data.py)

- [calucate dwt matrices in parallel](calculate_dtw_matrice.py)

- [calculate dwt matrice of repeated series in parallel](calculate_dtw_matrice_repeated_serie.py)

- [test if the in in parallel calculated matrices are correcly reassembled](test_dwt_matrice.py)

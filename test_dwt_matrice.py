#test if the in in parallel calculated matrices are distributed correcly

import pickle
import numpy
import numpy as np
from scipy.cluster.hierarchy import single, complete, average, ward, dendrogram
from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import fcluster
import sys

dwt_mat_full = None
for i in range(30):
    with open('denner_daten_full_weather/dwt-matrix_'+str(i)+'.pickle', 'rb') as handle:

        dwt_mat = pickle.load(handle)
        if dwt_mat_full is None:
            dwt_mat_full = np.zeros(dwt_mat.shape)
        dwt_mat_full += dwt_mat
print('number of fields',dwt_mat_full.shape[0]*dwt_mat_full.shape[1])
print('assumed non zero fields',(dwt_mat_full.shape[0]*dwt_mat_full.shape[1]-dwt_mat_full.shape[1])/2)
print('calculated non zero-fields',np.count_nonzero(dwt_mat_full))
dwt_mat_full = np.transpose(dwt_mat_full) + dwt_mat_full

# get rid of infinite values
dwt_mat_full[dwt_mat_full > sys.float_info.max] = -sys.float_info.max
max_value = np.max(dwt_mat_full)
dwt_mat_full[dwt_mat_full <0] = max_value
with open('denner_daten_full_weather/dwt-matrix_full.pickle', 'wb') as handle:
    pickle.dump(dwt_mat_full, handle, protocol=pickle.HIGHEST_PROTOCOL)

def hierarchical_clustering(dist_mat, method='complete'):
    if method == 'complete':
        Z = complete(dist_mat)
    if method == 'single':
        Z = single(dist_mat)
    if method == 'average':
        Z = average(dist_mat)
    if method == 'ward':
        Z = ward(dist_mat)

    fig = plt.figure(figsize=(16, 8))
    dn = dendrogram(Z)
    plt.title(f"Dendrogram for {method}-linkage with correlation distance")
    plt.show()

    return Z


linkage_matrix = hierarchical_clustering(dwt_mat_full,method='ward')


for i in [2,4,8,16,32,64,128]:
    cluster_labels = fcluster(linkage_matrix, i, criterion='maxclust')
    plt.title('No Of Clusters: '+str(i))
    plt.hist(cluster_labels,bins=i)
    print(np.histogram(cluster_labels,bins=i)[0]/len(cluster_labels))
    plt.show()
print(cluster_labels)
print(len(cluster_labels))

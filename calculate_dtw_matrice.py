#calucate dwt matrices in parallel

import pandas as pd
import pickle
import numpy as np
from dtaidistance import dtw
import time
from multiprocessing import Process
from window_generator import WindowGenerator
with open('denner_daten_full/frames.pickle', 'rb') as handle:
    frames = pickle.load(handle)

ts_list = []
for frame in frames:
    wg = WindowGenerator(frame, frame,item_no=frame['ItemNo'].iloc[0],steps=1,use_standard_scaling=True,lookback=0)
    wg.calc_window()
    _,y = wg.get_xy_train()
    ts_list.append(y.OrderedQuantity.to_numpy())
    print('added',frame['ItemNo'].iloc[0])

# The dtw_distance function expects series to be shaped as a (l, m) array,
# where l=length of series, m=# dimensions
series_list = ts_list

for _i in range(len(series_list)):
    length = len(series_list[_i])

    series_list[_i] = series_list[_i].reshape(length).astype(np.double)
print(len(series_list))
number_of_series = len(series_list)

start = time.time()
number_of_parallel_running_tasks = 30
shuffled_series_list = np.arange(number_of_series)
np.random.shuffle(shuffled_series_list)
chunks = np.array_split(shuffled_series_list, number_of_parallel_running_tasks)
print(chunks)

def run_in_parallel(series, no, chunk_to_calc):
    counter = 1
    distance_mat = np.zeros((number_of_series, number_of_series))
    for i in range(number_of_series):
        for j in range(i, number_of_series):
            if i not in chunk_to_calc.tolist():
                continue
            if i == j:
                continue
            print("calc ", no, i, j)
            if series[i].shape[0] == 0 or series[j].shape[0] ==0:
                distance_mat[i, j] = -np.inf
            else:
                distance_mat[i, j] = dtw.distance_fast(series[i], series[j], use_pruning=True)
            counter += 1
            if counter % 100 == 0:
                with open('denner_daten_full_weather/dwt-matrix_' + str(no) + '.pickle', 'wb') as handle:
                    pickle.dump(distance_mat, handle, protocol=pickle.HIGHEST_PROTOCOL)

    with open('denner_daten_full_weather/dwt-matrix_' + str(no) + '.pickle', 'wb') as handle:
        pickle.dump(distance_mat, handle, protocol=pickle.HIGHEST_PROTOCOL)
    print(distance_mat)


tasks = []
for _i in range(number_of_parallel_running_tasks):
    task = Process(target=run_in_parallel, args=(series_list, _i, chunks[_i]))
    tasks.append(task)
    task.start()
for task in tasks:
    task.join()
end = time.time()
print(end - start)

from sklearn.base import TransformerMixin, BaseEstimator
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt


class QueryByItemNoTransformer(TransformerMixin, BaseEstimator):
    def __init__(self, item_no):
        self.item_no = item_no

    def transform(self, input_df, **transform_params):
        item_no = self.item_no

        if (item_no != None):
            input_df = input_df.query("ItemNo == @item_no").copy()
        else:
            input_df = input_df.copy()
        return input_df

    def fit(self, X, y=None, **fit_params):
        return self


class RenameColumnsTransformer(TransformerMixin, BaseEstimator):
    def transform(self, input_df, **transform_params):
        return input_df.rename(columns={"Week Day": "Weekday"})

    def fit(self, X, y=None, **fit_params):
        return self


class ShiftColumnsTransformer(TransformerMixin, BaseEstimator):
    def __init__(self, shift, use_weather_data):
        self.shift = shift
        self.use_weather_data = use_weather_data

    def transform(self, input_df, **transform_params):
        input_df["OrderedQuantityToday"] = input_df["OrderedQuantity"]
        input_df["OrderedQuantity"] = input_df.shift(-self.shift)["OrderedQuantity"]
        input_df["OrderDateDeltaUnitPrice"] = input_df.shift(-self.shift)["DeltaUnitPrice"]
        input_df["OrderDateUnitPrice"] = input_df.shift(-self.shift)["UnitPrice"]
        input_df["OrderDateNumberOfSundaysAfter"] = input_df.shift(-self.shift)["NumberOfSundaysAfter"]
        input_df["OrderDateNumberOfSundaysBefore"] = input_df.shift(-self.shift)["NumberOfSundaysBefore"]
        input_df["OrderDateDaysInYear"] = input_df.shift(-self.shift)["DaysInYear"]
        input_df["OrderDateWeekday"] = input_df.shift(-self.shift)["Weekday"]
        input_df["OrderDateWeekNumber"] = input_df.shift(-self.shift)["WeekNumber"]
        input_df["OrderDateWeekSin"] = input_df.shift(-self.shift)["WeekSin"]
        input_df["OrderDateWeekCos"] = input_df.shift(-self.shift)["WeekCos"]
        input_df["OrderDateWeekNoSin"] = input_df.shift(-self.shift)["WeekNoSin"]
        input_df["OrderDateWeekNoCos"] = input_df.shift(-self.shift)["WeekNoCos"]
        input_df["OrderDateYearSin"] = input_df.shift(-self.shift)["YearSin"]
        input_df["OrderDateYearCos"] = input_df.shift(-self.shift)["YearCos"]

        if self.use_weather_data:
            input_df["OrderDateMaxTemp"] = input_df.shift(-self.shift)["MaxTemp"]
            input_df["OrderDateTotalPrecip"] = input_df.shift(-self.shift)["TotalPrecip"]

        return input_df.dropna().copy()

    def fit(self, X, y=None, **fit_params):
        return self


class GroupByShipmentDateTransformer(TransformerMixin, BaseEstimator):

    def transform(self, input_df, **transform_params):
        input_df["Timestamp"] = input_df["Shipment Date"]
        totalOrderedQuantity = input_df.groupby(["Timestamp"])["OrderedQuantity"].sum()
        input_df = input_df.groupby(["Timestamp"]).mean(numeric_only=True)
        input_df["OrderedQuantity"] = totalOrderedQuantity
        return input_df.sort_values(by=['Timestamp'])

    def fit(self, X, y=None, **fit_params):
        return self


class DatasetSplitTransformer(TransformerMixin, BaseEstimator):
    def transform(self, input_df, **transform_params):
        item_train_data_x = input_df.drop(["OrderedQuantity"], axis=1)
        item_train_data_y = input_df[["OrderedQuantity"]].copy()
        return item_train_data_x, item_train_data_y

    def fit(self, X, y=None, **fit_params):
        return self


class DropUnusedColumnsTransformer(TransformerMixin, BaseEstimator):
    def __init__(self, use_weather_data):
        self.use_weather_data = use_weather_data

    def transform(self, input_df):
        input_df = input_df.drop(input_df.filter(
            ["Quantity", "ShipmentYear", "Grund Mengenabweichung", "Mengenabweichung", "SellToCustomerNo",
             "Sammelpreisliste"]), axis=1)
        droppable_weather_columns = ["AvgHumidity", "MaxWind", "MinTemp", "AvgTemp"]
        if input_df.columns.isin(droppable_weather_columns).all():
            input_df = input_df.drop(droppable_weather_columns, axis=1)
        if input_df.columns.isin(["MaxTemp", "TotalPrecip"]).all() and not self.use_weather_data:
            input_df = input_df.drop(["MaxTemp", "TotalPrecip"], axis=1)

        return input_df.dropna()

    def fit(self, X):
        return self


# save mean and std of quantity of for inverse transform
class StandardScalingTransformer(TransformerMixin, BaseEstimator):

    def __init__(self):
        self.scaler = StandardScaler()
        self.quantity_mean = 0
        self.quantity_std = 1

    def transform(self, input_df):
        columns = input_df.columns
        df_scaled = self.scaler.transform(input_df.to_numpy())
        qantity_feature_no = columns.get_loc("OrderedQuantity")
        self.quantity_mean = self.scaler.mean_[qantity_feature_no]
        self.quantity_std = self.scaler.scale_[qantity_feature_no]

        return pd.DataFrame(df_scaled, columns=columns)

    def fit(self, X):
        self.scaler.fit_transform(X.to_numpy())
        return self


class HistoricTransformer(TransformerMixin, BaseEstimator):
    def __init__(self, length_of_slope, use_weather_data):
        self.length_of_slope = length_of_slope
        self.use_weather_data = use_weather_data

    def transform(self, input_df, **transform_params):
        input_df = input_df.copy()
        for i in range(1, self.length_of_slope + 1):
            input_df["OrderedQuantityToday" + str(i)] = input_df.shift(i)["OrderedQuantityToday"]
            # input_df["NextOrderIn"+str(i)] = input_df.shift(i-1)["DaysInYear"] - input_df.shift(i)["DaysInYear"]
            input_df["UnitPrice" + str(i)] = input_df.shift(i)["UnitPrice"]
            input_df["DeltaUnitPrice" + str(i)] = input_df.shift(i)["DeltaUnitPrice"]
            input_df["NumberOfSundaysAfter" + str(i)] = input_df.shift(i)["NumberOfSundaysAfter"]
            input_df["NumberOfSundaysBefore" + str(i)] = input_df.shift(i)["NumberOfSundaysBefore"]
            input_df["Weekday" + str(i)] = input_df.shift(i)["Weekday"]
            input_df["WeekNumber" + str(i)] = input_df.shift(i)["WeekNumber"]
            input_df["DaysInYear" + str(i)] = input_df.shift(i)["DaysInYear"]
            input_df["WeekCos" + str(i)] = input_df.shift(i)["WeekCos"]
            input_df["WeekSin" + str(i)] = input_df.shift(i)["WeekSin"]
            input_df["WeekNoSin" + str(i)] = input_df.shift(i)["WeekNoSin"]
            input_df["WeekNoCos" + str(i)] = input_df.shift(i)["WeekNoCos"]
            input_df["YearSin" + str(i)] = input_df.shift(i)["YearSin"]
            input_df["YearCos" + str(i)] = input_df.shift(i)["YearCos"]

            if self.use_weather_data:
                # input_df["MinTemp"+str(i)] = input_df.shift(i)["MinTemp"]
                input_df["MaxTemp" + str(i)] = input_df.shift(i)["MaxTemp"]
                # input_df["AvgTemp"+str(i)] = input_df.shift(i)["AvgTemp"]
                # input_df["MaxWind"+str(i)] = input_df.shift(i)["MaxWind"]
                input_df["TotalPrecip" + str(i)] = input_df.shift(i)["TotalPrecip"]
                # input_df["AvgHumidity"+str(i)] = input_df.shift(i)["AvgHumidity"]
            input_df = input_df.copy()
        return input_df.dropna().copy()

    def fit(self, X, y=None, **fit_params):
        return self


class FillMissingValuesTransformer(TransformerMixin, BaseEstimator):
    def __init__(self, use_weather_data):
        self.use_weather_data = use_weather_data

    def transform(self, input_df, **transform_params):
        input_df = input_df.asfreq('D')
        input_df['Shipment Date'] = input_df.index
        input_df['Weekday'] = input_df['Shipment Date'].dt.dayofweek
        input_df['DaysInYear'] = input_df['Shipment Date'].dt.day_of_year
        input_df['UnitPrice'] = input_df['UnitPrice'].fillna(method='bfill')
        input_df['OrderedQuantity'] = input_df['OrderedQuantity'].fillna(method='bfill')
        if self.use_weather_data:
            input_df['MaxTemp'] = input_df['MaxTemp'].fillna(method='bfill')
            input_df['TotalPrecip'] = input_df['TotalPrecip'].fillna(method='bfill')
        input_df['WeekNumber'] = input_df['Shipment Date'].dt.isocalendar().week

        week = 7 * 24 * 60 * 60
        year = 60 * 40 * 24 * 365.25
        weeks_per_year = 53
        timestamp_s = input_df['Shipment Date'].map(pd.Timestamp.timestamp)
        input_df['WeekSin'] = np.sin(timestamp_s * (2 * np.pi / week))
        input_df['WeekCos'] = np.cos(timestamp_s * (2 * np.pi / week))
        input_df['YearSin'] = np.sin(timestamp_s * (2 * np.pi / year))
        input_df['YearCos'] = np.cos(timestamp_s * (2 * np.pi / year))
        input_df['WeekNoSin'] = np.sin(input_df['WeekNumber'] * (2 * np.pi / weeks_per_year))
        input_df['WeekNoCos'] = np.cos(input_df['WeekNumber'] * (2 * np.pi / weeks_per_year))

        # input_df['isHoliday'] = input_df['Shipment Date'] in swiss_holidays
        # input_df['isSunday'] = input_df['Shipment Date'].dt.dayofweek == 6
        input_df['NumberOfSundaysBefore'] = input_df['NumberOfSundaysBefore'].fillna(0)
        input_df['NumberOfSundaysAfter'] = input_df['NumberOfSundaysAfter'].fillna(0)
        input_df = input_df.drop(['Shipment Date'], axis=1)
        return input_df

    def fit(self, x):
        return self


class DeltaUnitPriceTransformer(TransformerMixin, BaseEstimator):

    def transform(self, input_df, **transform_params):
        input_df['DeltaUnitPrice'] = input_df['UnitPrice'].shift(1) - input_df['UnitPrice']
        input_df['DeltaUnitPrice'] = input_df['DeltaUnitPrice'].fillna(0)

        return input_df

    def fit(self, x):
        return self


class MovingAverageTransformer(TransformerMixin, BaseEstimator):
    def __init__(self, length_of_moving_average, affected_columns=["OrderedQuantity"]):
        self.length_of_moving_average = length_of_moving_average
        self.affected_columns = affected_columns

    def transform(self, input_df, **transform_params):
        input_df[self.affected_columns] = input_df[self.affected_columns].rolling(self.length_of_moving_average,
                                                                                  min_periods=1).mean()
        return input_df

    def fit(self, x):
        return self


class ExponentialMovingAverageTransformer(TransformerMixin, BaseEstimator):
    def __init__(self, alpha, adjust=False, affected_columns=["OrderedQuantity"]):
        self.alpha = alpha
        self.adjust = adjust
        self.affected_columns = affected_columns

    def transform(self, input_df, **transform_params):
        input_df[self.affected_columns] = input_df[self.affected_columns].ewm(alpha=self.alpha,
                                                                              adjust=self.adjust).mean()
        return input_df

    def fit(self, x):
        return self


from scipy.signal import medfilt


class MedianFilterTransformer(TransformerMixin, BaseEstimator):
    def __init__(self, kernel_size=3, affected_columns=["OrderedQuantity"]):
        self.kernel_size = kernel_size
        self.affected_columns = affected_columns

    def transform(self, input_df, **transform_params):
        # do not use mefilter -> medfilter is looking forward

        med_column = input_df[self.affected_columns].copy()
        for i in range(1, input_df.shape[0]):
            if i < self.kernel_size:
                window = i
            else:
                window = self.kernel_size
            med = np.median(input_df[self.affected_columns].iloc[i - window:i])
            med_column.iloc[i] = med
        input_df[self.affected_columns] = med_column

        # input_df["OrderedQuantity"] = medfilt(input_df["OrderedQuantity"],kernel_size=self.kernel_size)
        return input_df

    def fit(self, x):
        return self


# baseline model
import numpy as np
import pandas as pd
from sklearn.pipeline import Pipeline


class WindowGenerator():

    def __init__(self, train_data, test_data, item_no=None, lookback=1, offset=1, steps=1, predict_moving_average=False,
                 use_weather_data=False, use_standard_scaling=False, moving_average_transformer=None,
                 only_used_features=None, embedding=None):
        self.train_data = train_data
        self.test_data = test_data
        self.item_no = None
        self.lookback = lookback
        self.offset = offset
        self.steps = steps
        self.predict_moving_average = predict_moving_average
        self.use_weather_data = use_weather_data
        self.use_standard_scaling = use_standard_scaling
        self.item_no = item_no
        self.moving_average_transformer = moving_average_transformer
        self.only_used_features = only_used_features
        self.embedding = embedding

    def set_train_data(self, train_data):
        self.train_data = train_data

    def set_test_data(self, test_data):
        self.test_data = test_data

    def set_item_query(self, item_no):
        self.item_no = item_no

    def get_xy_train(self):
        return self.train_x, self.train_y

    def get_xy_test(self):
        return self.test_x, self.test_y

    def calc_window(self):
        self.train_data = self.train_data.filter(self.test_data.columns)
        self.test_data = self.test_data.filter(self.train_data.columns)
        self._calc_window(self.train_data, False)
        self._calc_window(self.test_data, True)

    def get_feature_tensor_test(self):
        return self._get_feature_tensor(self.test_x)

    def get_feature_tensor_train(self):
        return self._get_feature_tensor(self.train_x)

    def get_feature_tensor2_test(self):
        return self._get_feature_tensor2(self.test_x)

    def get_feature_tensor2_train(self):
        return self._get_feature_tensor2(self.train_x)

    def _calc_window(self, data, is_test_set):
        panda_frame_pipeline = self._get_pipeline(is_test_set)
        if not is_test_set:
            x, y = panda_frame_pipeline.fit_transform(data)
        else:
            x, y = panda_frame_pipeline.transform(data)

        x = HistoricTransformer(self.lookback, self.use_weather_data).fit_transform(x)
        y = y.shift(-self.lookback).dropna()

        if self.predict_moving_average:
            for i in range(1, self.steps):
                _y = y.copy()
                _x = x.copy()
                _y = _y.shift(-i)
                _x = _x.shift(-i)
                y["OrderedQuantity"] += _y["OrderedQuantity"]
                x["OrderDateWeekday"] += _x["OrderDateWeekday"]
                x["OrderDateUnitPrice"] += _x["OrderDateUnitPrice"]
                x["OrderDateDeltaUnitPrice"] += _x["OrderDateDeltaUnitPrice"]
                x["OrderDateWeekNumber"] += _x["OrderDateWeekNumber"]
                x["OrderDateNumberOfSundaysAfter"] += _x["OrderDateNumberOfSundaysAfter"]
                x["OrderDateNumberOfSundaysBefore"] += _x["OrderDateNumberOfSundaysBefore"]
            y["OrderedQuantity"] /= self.steps

        else:

            for i in range(1, self.steps):
                _y = y.copy()
                _x = x.copy()
                _y = _y.shift(-i)
                _x = _x.shift(-i)
                y["OrderedQuantity" + str(i)] = _y["OrderedQuantity"]
                x["OrderDateUnitPrice" + str(i)] = _x["OrderDateUnitPrice"]
                x["OrderDateNumberOfSundaysAfter" + str(i)] = _x["OrderDateNumberOfSundaysAfter"]
                x["OrderDateNumberOfSundaysBefore" + str(i)] = _x["OrderDateNumberOfSundaysBefore"]
                x["OrderDateWeekday" + str(i)] = _x["OrderDateWeekday"]
                x["OrderDateDeltaUnitPrice" + str(i)] = _x["OrderDateDeltaUnitPrice"]
                x["OrderDateWeekNumber" + str(i)] = _x["OrderDateWeekNumber"]
                x["OrderDateDaysInYear" + str(i)] = _x["OrderDateDaysInYear"]
                x["OrderDateWeekCos" + str(i)] = _x["OrderDateWeekCos"]
                x["OrderDateWeekSin" + str(i)] = _x["OrderDateWeekSin"]
                x["OrderDateWeekNoCos" + str(i)] = _x["OrderDateWeekNoCos"]
                x["OrderDateWeekNoSin" + str(i)] = _x["OrderDateWeekNoSin"]
                x["OrderDateYearCos" + str(i)] = _x["OrderDateYearCos"]
                x["OrderDateYearSin" + str(i)] = _x["OrderDateYearSin"]
                if self.use_weather_data:
                    x["OrderDateMaxTemp" + str(i)] = _x["OrderDateMaxTemp"]
                    x["OrderDateTotalPrecip" + str(i)] = _x["OrderDateTotalPrecip"]
                # x["NextOrderIn"+str(i)] = _x["NextOrderIn"] +x["NextOrderIn"]
                # replace NextOrderIn with OrderDayOfYear
                x = x.copy()
        x = x.dropna()
        y = y.dropna()
        if is_test_set:
            self.test_x = x
            self.test_y = y
        else:
            self.train_x = x
            self.train_y = y
        return x, y

    def get_future_info(df, iloc, predicted_quantity):
        next_entry = df.iloc[iloc + 1]
        next_entry["OrderedQuantityToday"] = predicted_quantity
        return next_entry

    def _get_pipeline(self, is_test_set):

        transformations = [
            ('rename columns', RenameColumnsTransformer()),
            ('query_by_item_no', QueryByItemNoTransformer(self.item_no)),
            ('drop_unused_columns', DropUnusedColumnsTransformer(self.use_weather_data)),
            # every non-numeric column will be dropped
            ('group_by_shipmentdate', GroupByShipmentDateTransformer()),
            ('Fill Missing Values', FillMissingValuesTransformer(self.use_weather_data)),
            ('Add DeltaUnitPrice', DeltaUnitPriceTransformer()),
            ('shift_columns', ShiftColumnsTransformer(self.offset, self.use_weather_data)),
            ('dataset_split_transformer', DatasetSplitTransformer())]

        if self.use_standard_scaling:
            # only calculating scaler for train set
            if not is_test_set:
                self.standard_scaling = StandardScalingTransformer()
            transformations.insert(5, ('standard_scaling', self.standard_scaling))

        if self.moving_average_transformer is not None:
            transformations.insert(6, ('moving_average_transformer', self.moving_average_transformer))

        return Pipeline(transformations)

    def inverse_transform(self, y):
        if self.use_standard_scaling:
            return (y * self.standard_scaling.quantity_std) + self.standard_scaling.quantity_mean
        return y

    def _get_feature_tensor(self, x):
        columns = ["OrderedQuantityToday", "DeltaUnitPrice", "UnitPrice", "NumberOfSundaysAfter",
                   "NumberOfSundaysBefore", "Weekday", "DaysInYear", "WeekNumber", "WeekCos", "WeekSin", "YearSin",
                   "YearCos", "WeekNoSin", "WeekNoCos"]
        if self.use_weather_data:
            columns += ["MaxTemp", "TotalPrecip"]

        if self.only_used_features is not None:
            columns = list(set(columns).intersection(set(self.only_used_features)))

        conv_today = x[columns].to_numpy()[:, np.newaxis, ...]

        for i in range(1, self.lookback + 1):
            prev_columns = [column + str(i) for column in columns]
            conv_prev_day = x[prev_columns].to_numpy()[:, np.newaxis, ...]
            conv_today = np.concatenate((conv_prev_day, conv_today), axis=1)

        if self.embedding is not None:
            v = self.embedding.get_embedding_vector(self.item_no)
            v = v.reshape(1, v.shape[1])
            v = v.repeat(conv_today.shape[1], axis=0)
            v = v.reshape(1, v.shape[0], v.shape[1])
            v = v.repeat(conv_today.shape[0], axis=0)

            conv_today = np.concatenate((conv_today, v), axis=2)

        return conv_today

    def _get_feature_tensor2(self, x):
        columns = ["OrderDateUnitPrice", "OrderDateNumberOfSundaysBefore", "OrderDateNumberOfSundaysAfter",
                   "OrderDateWeekday", "OrderDateDeltaUnitPrice", "DaysInYear", "OrderDateWeekNumber",
                   "OrderDateWeekCos", "OrderDateWeekSin", "OrderDateSinYear", "OrderDateCosYear", "OrderDateWeekNoSin",
                   "OrderDateWeekNoCos"]
        if self.use_weather_data:
            columns += ["OrderDateMaxTemp", "OrderDateTotalPrecip"]

        if self.only_used_features is not None:
            columns = list(set(columns).intersection(set(self.only_used_features)))
        column_list = columns.copy()
        for i in range(1, self.steps):
            column_list += [column + str(i) for column in columns]

        return x[column_list].copy()

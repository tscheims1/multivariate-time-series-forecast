class ModelConfig:

    def __init__(self, model_name, embedding, lookback, is_2_head=False):
        self.embedding = embedding
        self.lookback = lookback
        self.model_name = model_name
        self.is_2_head = is_2_head

# Script for merging and adding the exported sales shipment lines to a pandas object
# the merging is necessary, because the item numbering has changed in the Microsoft Business Central ERP and merging the data while exporting it, takes to much time

import pickle
import glob
import pandas as pd
import re

base_path = 'denner_daten_full_weather'
files = glob.glob(base_path+'/*xlsx')
files.sort()

frames = []
pattern = '.*_(.*)_.*'
for idx in range(len(files)):
    frame = pd.read_excel(files[idx])
    if idx >= 1 and re.sub(pattern, '\\1', files[idx]) == re.sub(pattern, '\\1', files[idx-1]):
        frames[-1] = pd.concat([frames[-1], frame])
        print(files[idx], 'concated')
    else:
        frames.append(frame)
        print(files[idx], 'added')


with open(base_path+'/frames.pickle', 'wb') as handle:
    pickle.dump(frames, handle, protocol=pickle.HIGHEST_PROTOCOL)

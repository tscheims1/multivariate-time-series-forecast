import tensorflow as tf
import numpy as np


class Embedding:

    def __init__(self, frames, output_dim):
        self.items = []
        self.output_dim = output_dim
        for frame in frames:
            self.items.append(frame['ItemNo'].min())

        self.model = tf.keras.Sequential()
        self.model.add(tf.keras.layers.Embedding(len(self.items) + 1, self.output_dim, input_length=1))
        self.model.compile('rmsprop', 'mse')

    def get_embedding_vector(self, item_no):
        if not item_no in self.items:
            self.items.append(item_no)
        return self.model.predict(np.array(self.items.index(item_no)).reshape(1, 1), verbose=False)[0]

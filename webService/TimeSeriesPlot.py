from matplotlib import pyplot as plt
import numpy as np
import io
import base64

class TimeSeriesPlot:

    def get_base64_plot(self, prediction_result):
        plt.plot(np.arange(len(prediction_result.input_sequence)), prediction_result.input_sequence, c='b')
        plt.plot(np.arange(len(prediction_result.input_sequence) - 1,
                           len(prediction_result.input_sequence) + len(prediction_result.prediction_sequence)),
                 [prediction_result.input_sequence[-1]] + prediction_result.prediction_sequence, c='r')
        plt.axvline(len(prediction_result.input_sequence)-1)
        if prediction_result.test_sequence is not None:
            plt.plot(np.arange(len(prediction_result.input_sequence) - 1,
                               len(prediction_result.input_sequence) + len(prediction_result.test_sequence)),
                     [prediction_result.input_sequence[-1]] + prediction_result.test_sequence, c='b')
        plt.legend(['actual', 'prediction'])
        s = io.BytesIO()
        plt.savefig(s, format='png')
        plt.close()
        s.seek(0)
        return base64.b64encode(s.read()).decode('utf-8')

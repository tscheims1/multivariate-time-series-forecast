#calculate average swiss weather and merge it to existing pandas frames

import datetime
import os
import json
import pandas as pd
import glob
import re
from pathlib import Path
base_dir = "weather"
locations = ["Bern", "Basel", "Zürich", "Lausanne", "Lugano", "Chur Switzerland", "Sion", "St. Gallen", "Luzern",
             "Delsberg"]


class WeatherData():
    def __init__(self, date):
        self.date = date
        self.min_temp = 0
        self.max_temp = 0
        self.avg_temp = 0
        self.total_precip = 0
        self.avg_humidity = 0
        self.max_wind = 0

    def __str__(self):
        return str(self.date) + "\n" + str(self.min_temp) + "\n" + str(self.max_temp) + "\n" + str(
            self.avg_temp) + "\n" + str(self.total_precip) + "\n" + str(self.avg_humidity) + "\n" + str(self.max_wind)

    def __repr__(self):
        return self.__str__()
class WeatherDataAugmenter():
    def __init__(self):
        self.weather_data = []
    def load(self):
        curr_date = datetime.datetime(2018, 1, 1)
        end_date = datetime.datetime(2022, 12, 22)

        while curr_date != end_date:
            wd = WeatherData(curr_date)
            for loc in locations:
                dir = os.path.join(base_dir, loc)
                file_path = os.path.join(dir, curr_date.strftime("%Y-%m-%d")) + ".json"
                with open(file_path) as f:
                    content = f.read()
                json_content = json.loads(content)
                wd.min_temp += float(json_content["forecast"]["forecastday"][0]["day"]["mintemp_c"])
                wd.max_temp += float(json_content["forecast"]["forecastday"][0]["day"]["maxtemp_c"])
                wd.avg_temp += float(json_content["forecast"]["forecastday"][0]["day"]["avgtemp_c"])
                wd.total_precip += float(json_content["forecast"]["forecastday"][0]["day"]["totalprecip_mm"])
                wd.avg_humidity += float(json_content["forecast"]["forecastday"][0]["day"]["avghumidity"])
                wd.max_wind += float(json_content["forecast"]["forecastday"][0]["day"]["maxwind_kph"])
            wd.min_temp /= len(locations)
            wd.max_temp /= len(locations)
            wd.avg_temp /= len(locations)
            wd.total_precip /= len(locations)
            wd.avg_humidity /= len(locations)
            wd.max_wind /= len(locations)
            self.weather_data.append(wd)
            curr_date += datetime.timedelta(days=1)

    def augment(self,excel_file):
        df = pd.read_excel(excel_file)

        for wd in self.weather_data:
            df.loc[df["Shipment Date"] == wd.date, "MinTemp"] = wd.min_temp
            df.loc[df["Shipment Date"] == wd.date, "MaxTemp"] = wd.max_temp
            df.loc[df["Shipment Date"] == wd.date, "AvgTemp"] = wd.avg_temp
            df.loc[df["Shipment Date"] == wd.date, "MaxWind"] = wd.max_wind
            df.loc[df["Shipment Date"] == wd.date, "TotalPrecip"] = wd.total_precip
            df.loc[df["Shipment Date"] == wd.date, "AvgHumidity"] = wd.avg_humidity

        save_dir = Path(excel_file).parent.absolute()
        filename = os.path.basename(excel_file)
        df.to_excel(os.path.join(save_dir, "weather_"+filename),index=False)
        print(filename, "saved")

wa = WeatherDataAugmenter()
wa.load()
for file in glob.glob("denner_daten_full/*xlsx"):
    pattern = re.compile('.*weather.*')
    if not pattern.match(file):
        wa.augment(file)

